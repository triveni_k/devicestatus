export class ErrorFactory {
   static getErrors(ex: any) {
        switch (ex.code) {
            case 12009 || "E11000":
                return {Error: "record already exists"};
            case 12003:
                return {Error: "invalid collection"};
            case 2:
                return {Error: "DbAuthentication failed"};
            default:
                return {Error: ex.message};
        }
    }
}