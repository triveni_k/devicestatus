﻿var commonErrors = {
  // Invalid Request: Requests which fail unique index (unique-key) constraint
  CARTOS_ERROR_RECORD_ALREADY_EXISTS: {
    code: "CARTOS_ERROR_RECORD_ALREADY_EXISTS",
    message: "Record Already Exists."
  },
  // Invalid Request: Requests which fail business validation
  CARTOS_ERROR_INVALID_REQUEST: {
    code: "CARTOS_ERROR_INVALID_REQUEST",
    message: "Request is invalid. Please check info field for more details."
  },
  CARTOS_ERROR_INVALID_SESSION: {
    code: "CARTOS_ERROR_INVALID_SESSION",
    message: "Session does not exists."
  },
  CARTOS_ERROR_NO_RECORD: {
    code: "CARTOS_ERROR_NO_RECORD",
    message: "No matching records."
  },

  CARTOS_ERROR_PARTIAL_EXECUTION: {
    code: "CARTOS_ERROR_PARTIAL_EXECUTION",
    message: "Some records in the request could not be processed."
  },
  CARTOS_ERROR_SESSION_EXPIRED: {
    code: "CARTOS_ERROR_SESSION_EXPIRED",
    message: "Session expired. Please login again."
  },
  CARTOS_ERROR_OTHER: {
    code: "CARTOS_ERROR_OTHER",
    message: "Unknown Error occured. Please try again or contact System Administrator."
  },
  CARTOS_ERROR_UNAUTHORIZED_DATABASE_ACCESS: {
    code: "CARTOS_ERROR_UNAUTHORIZED_DATABASE_ACCESS",
    message: "Error in connecting to database. Please contact System Administrator."
  },
  CARTOS_ERROR_DATABASE: {
    code: "CARTOS_ERROR_DATABASE",
    message: "Error interacting with database. Please contact System Administrator."
  },
  CARTOS_ERROR_EXCEPTION: {
    code: "CARTOS_ERROR_EXCEPTION",
    message: "Unknown Error occured. Please try again or contact System Administrator."
  },
  CARTOS_ERROR_MISSING_CONFIGURATION: {
    code: "CARTOS_ERROR_MISSING_CONFIGURATION",
    message: "Some configuration in the service is missing. Please contact System Administrator."
  },
  CARTOS_ERROR_MAXIMUM_COUNT_EXCEEDED: {
    code: "CARTOS_ERROR_MAXIMUM_COUNT_EXCEEDED",
    message: "Maximum templates for user exceeded, please use or edit existing templates."
  },
  CARTOS_ERROR_UNAUTHORIZED: {
    code: "CARTOS_ERROR_UNAUTHORIZED",
    message: "Unauthorized to perform this action."
  },
  CARTOS_MAXIMUM_SIZE_EXCEEDS: {
    code: "CARTOS_MAXIMUM_SIZE_EXCEEDS",
    message: "Document exceeds maximum allowed bson size of 16777216 bytes"
  },
  CARTOS_EXT_SERVICE_UNAUTHORIZED: {
    code: "CARTOS_EXT_SERVICE_UNAUTHORIZED",
    message: "Unauthorized to perform this action."
  },
  CARTOS_EXT_SERVICE_ERROR: {
    code: "CARTOS_EXT_SERVICE_ERROR",
    message: "CouchBaseConnection refused. Please try again or contact System Administrator."
  },
  CARTOS_SENECA_ERROR: {
    code: "CARTOS_SENECA_ERROR",
    message: "Seneca error, request failed"
  },
  CARTOS_INFECTED_FILE_ERROR: {
    code: "CARTOS_INFECTED_FILE_ERROR",
    message: "The multiplying villanies of nature do swarm upon us."
  }
};

exports.Errors = function Errors() {
  return commonErrors;
};
