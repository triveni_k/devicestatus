﻿"use strict";

var responseBuilder = require("./responseBuilder");
var errors = require("../errorHandler/errors").Errors();
var eventLogger = require("../config/eventLogger/eventLoggerFactory").EventLoggerFactory.getEventLogger();
var eventLoggerObject = new eventLogger();
var hTTPStatus = require("http-status-codes");
var logger = require("../config/logger");
var log = logger.LOG;
var stream = require("stream");
var zlib = require("zlib");

class ResponseHandler {
  getStatusByErrorCode(code: number) {
    switch (code) {
      case errors.CARTOS_ERROR_NO_RECORD.code: //return 204
        return hTTPStatus.NO_CONTENT;
        //return 401 (Unauthorized) is the session is not valid or expired
      case errors.CARTOS_ERROR_INVALID_SESSION.code:
        return hTTPStatus.UNAUTHORIZED;
      case errors.CARTOS_ERROR_SESSION_EXPIRED.code:
        return hTTPStatus.UNAUTHORIZED;
      case errors.CARTOS_ERROR_UNAUTHORIZED.code:
        return hTTPStatus.UNAUTHORIZED;
      case errors.CARTOS_ERROR_UNAUTHORIZED_DATABASE_ACCESS.code:
        return hTTPStatus.UNAUTHORIZED;
        //return 422 for invalid requests
        // Requests which fail unique index (unique-key) constraint
      case errors.CARTOS_ERROR_RECORD_ALREADY_EXISTS.code:
        return hTTPStatus.UNPROCESSABLE_ENTITY;
        // Requests which fail business validation
      case errors.CARTOS_ERROR_INVALID_REQUEST.code:
        return hTTPStatus.UNPROCESSABLE_ENTITY;
        //Return 500 for all other errors
      default:
        return hTTPStatus.INTERNAL_SERVER_ERROR;
    }
  }

  send(res: any, err: any, data: any, req: any) {
    var response = {};
    //calling function to log userSysId, method, url, err & data as exit of an user
    if (req && req.method !== "GET") {
      eventLoggerObject.emitExitEvent(res.userSysId, req.method, res.url, {err: err, data: data});
    }
    //Partial success is when both err and data is present
    if (err && data) {
      response = new responseBuilder.PartialSuccessResponse(err, data);
      res.status(hTTPStatus.PARTIAL_CONTENT).json(response);
    } else if (err) { //Complete Failure
      //Get http status for the response based on the error object
      var httpStatus = this.getStatusByErrorCode(err.errorCode);
      response = new responseBuilder.ErrorResponse(err);
      res.status(httpStatus).json(response);
    } else if (!err && !data) {  // No error and No data
      res.status(hTTPStatus.NO_CONTENT).json();
    } else { //Success
      try {
        response = new responseBuilder.SuccessResponse(data);
        // if (req && req.headers["accept-encoding"] && req.headers["accept-encoding"].indexOf("gzip") !== -1) {
        //   // Create readable stream
        //   var readStream = new stream.Readable;
        //   readStream.push(JSON.stringify(response));
        //   readStream.push(null); // Notify readStream to terminate the process
        //
        //   res.writeHead(hTTPStatus.OK, {"Content-Encoding": "gzip"}); // Generate response header
        //   readStream.pipe(zlib.createGzip()).pipe(res); // Data compression and sending back the response
        // } else {
        res.status(hTTPStatus.OK).json(response);
        // }
      } catch (ex) {
        log.error("Error while generating the API response - ", ex);
        res.status(hTTPStatus.INTERNAL_SERVER_ERROR).json();
      }
    }
  }
}

module.exports = {
  getStatusByErrorCode: new ResponseHandler().getStatusByErrorCode,
  send: new ResponseHandler().send
};