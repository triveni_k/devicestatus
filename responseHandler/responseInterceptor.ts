﻿var interceptor = require("res-interceptor");

class Interceptor {
  interceptor(req: any, res: any, next: any, data: any) {
    //Jwt's secretCallback function sets the new token in the req.header
    //Copy the new token from req.header to response header before sending response
    res.set("Authorization", req.headers["x-token"]);
  }
}

exports.interceptor = interceptor(new Interceptor().interceptor);
