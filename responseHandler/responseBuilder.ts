"use strict";
var baseResponse = require("./baseResponse"),
    errors = require("../errorHandler/errors").Errors(),
    util = require("util");

function PartialSuccessResponse(err: any, data: any) {
  //Inherit Base Response to get the common properties built into response
  //TODO: Use arguments to improvise on Base Response object when these will be implemented
  baseResponse.apply(this, arguments);
  this.data = data;
  this.error = err;
}
util.inherits(PartialSuccessResponse, baseResponse);

function ErrorResponse(err: any) {
  //Inherit Base Response to get the common properties built into response
  //TODO: Use arguments to improvise on Base Response object when these will be implemented
  baseResponse.apply(this, arguments);
  this.error = err;
}
util.inherits(ErrorResponse, baseResponse);

function SuccessResponse(data: any) {
  //Inherit Base Response to get the common properties built into response
  //TODO: Use arguments to improvise on Base Response object when these will be implemented
  baseResponse.apply(this, arguments);
  this.data = data;
}
util.inherits(SuccessResponse, baseResponse);

module.exports = {
  //Constructors for different types of Response Builder Objects
  PartialSuccessResponse: PartialSuccessResponse,
  ErrorResponse: ErrorResponse,
  SuccessResponse : SuccessResponse
};