import {DriverConnectionManager} from "./dbLayer/factory/IndexDB";
import {Register} from "./dbLayer/default/Register";

var swaggerExpress = require("swagger-express-mw");
var app = require("express")();
var fs = require("fs");
var https = require("https");
var config = require("./config/config");
var path = require("path");
var logger = require("./config/logger"),
    log = logger.LOG;
var bodyParser = require("body-parser");

module.exports = app; // for testing

var rootPath = path.normalize(__dirname);
config.appRoot = rootPath;

var options = {
    key: fs.readFileSync(rootPath + config.certKeyFilePath),
    cert: fs.readFileSync(rootPath + config.certFilePath),
    ciphers: config.ciphers,
    honorCipherOrder: config.serverConfig.honorCipherOrder,
    secureProtocol: config.serverConfig.secureProtocol
};
/*
var redFortSecretCallback = function (req: any, payload: any, done: any) {
    req.redfort = {
        userId: payload.userSysId,
        email: payload.email,
        orgId: payload.orgId,
        orgName: payload.orgName,
        profivarype: payload.profivarype,
        secretKey: payload.secretKey
    };
    done(null, payload.secret);
};*/

app.use(bodyParser.urlencoded({
    extended: true
}));
(new Register).initIdb();
app.use(bodyParser.json({limit: "15mb"}));

swaggerExpress.create(config, function (err: any, swaggerExpress: any) {
    if (err) {
        throw err;
    }

    /*redFortSdk.configure({
        cartos_itt: JwtConfig.getJWTConfiguration("cartos_itt"),
        cartos_tenant: JwtConfig.getJWTConfiguration("cartos_tenant"),
        cartos_local: JwtConfig.getJWTConfiguration("cartos_local"),
        serviceBaseUrl: config.RedFortServiceBaseUrl,
        requestOptions: {
            rejectUnauthorized: config.ssl.rejectUnAuthorizedCert
        }
    });
*/
    /*if (config.features.authorization) {
        app.use(config.apiPaths.version1,
            redFortSdk.auth.verify({secret: redFortSecretCallback}).unless({path: config.apiPaths.excludeAuthentication}));
    }*/

    // install middleware
    swaggerExpress.register(app);


    // app.listen(port);

    var server = https.createServer(options, app).listen(config.serverConfig.port);
    server.timeout = config.serverConfig.timeout;
    console.log("Listening on sample service: " + config.serverConfig.port);

//    register.registerDependencies();
    process.on("uncaughtException", function (err: any) {
        // This should not happen
        log.error("Pheew ...! Something unexpected happened. This should be handled more gracefully. I am sorry. The culprit is: ", err);
    });
});
