import {Snmp} from "../default/Snmp";

export interface IDeviceServiceConnection {
    createSnmpCon(ip: string): void;
    getDeviceProperties(ip: string, oids: Array<string>): any;
}