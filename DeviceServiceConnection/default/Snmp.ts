import {IDeviceServiceConnection} from "../contracts/IDeviceServiceConnection";
import {IConnectionManager} from "../../dbLayer/contracts/ConnectionManager";
import {Varbinds} from "../../dbLayer/models/Varbinds";
import {OidAndValue} from "../../dbLayer/models/oidAndValue";

let snmp = require("net-snmp");
let textEncoding = require("text-encoding");
let TextDecoder = textEncoding.TextDecoder;

export class Snmp implements IDeviceServiceConnection {
    private static instance: Snmp;
    private snmpConnections = new Map<string, any>();
    private session: Array<any>;
    private registerEvents: any;

    public static getInstance() {
        if (!Snmp.instance) {
            Snmp.instance = new Snmp();
        }
        return Snmp.instance;
    }

    static buildResponse(varbinds: Varbinds) {
        let res: any = {};
        if (varbinds.value.constructor === Buffer) {
            return new TextDecoder("ascii").decode(varbinds.value);
        } else {
            return varbinds.value;
        }
    }

    public async createSnmpCon(ip: string) {
        this.snmpConnections.set(ip, snmp.createSession(ip, "public"));
    }

    async getDeviceProperties(ip: string, oids: Array<string>) {
        let flag: boolean = true;
        return new Promise((resolve: Function, reject: Function) => {
            let session = this.snmpConnections.get(ip);
            session.get(oids, function (error: any, varbinds: any) {
                if (error) {
                    reject(error);
                } else {
                    let res: any = {};
                    for (var i = 0; i < varbinds.length; i++) {
                        if (snmp.isVarbindError(varbinds[i])) {
                            flag = false;
                        }
                        if (flag) {
                            res[varbinds[i].oid] = (Snmp.buildResponse(varbinds[i]));
                        } else {
                            reject(varbinds);
                        }
                    }
                    resolve(res);
                }
            });
        });
    }

    closeSnmpConnection() {
        snmp.closeConnection();
    }
}