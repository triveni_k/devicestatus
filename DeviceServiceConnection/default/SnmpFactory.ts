import {Snmp} from "./Snmp";
import {IDeviceServiceConnection} from "../contracts/IDeviceServiceConnection";
import {IDeviceServiceFactory} from "../contracts/IDeviceServiceFactory";

let config = require("../../config/config");

export class SnmpFactory implements IDeviceServiceFactory {
    private static instance: SnmpFactory;
    private deviceService = new Map<string, any>();

    static getInstance() {
        if (!SnmpFactory.instance) {
            SnmpFactory.instance = new SnmpFactory();
        }
        return SnmpFactory.instance;
    }

    public registerService(service: string) {
        this.deviceService.set(service, this.getInstances(service));
    }

    async getInstances(service: any) {
        if (service.snmp === config.deviceService.snmp) {
            return await Snmp.getInstance();
        }
    }

    getDeviceService(service: string) {
        return this.deviceService.get(service);
    }
}
