export enum OperationType {
  Delete,
  Post,
  Put
}