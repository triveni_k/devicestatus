import {Collection} from "../contracts/Collection";
import {DeviceProperty} from "../models/deviceProperties";
import {hasOwnProperty} from "tslint/lib/utils";
import {Options} from "tslint/lib/runner";

let config = require("../../config/config");

export class MongoOperation implements Collection {
    private static instance: MongoOperation;
    private connection: any;

    static getInstance() {
        if (!MongoOperation.instance) {
            MongoOperation.instance = new MongoOperation();
        }
        return MongoOperation.instance;
    }

    static intializeId(data: Array<{ [key: string]: any }>) {
        for (let index = 0; index < data.length; index++) {
            data[index]._id = data[index].assetId;
        }
    }

    setConnection(db: any) {
        this.connection = db;
    }

    public async insert(collection: string, data: Array<{ [key: string]: any }>) {
        MongoOperation.intializeId(data);
        return new Promise((resolve, reject) => {
            this.connection.collection(collection).insert(data, (err: any, result: any) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            });
        });
    }

    public async getDataFromId(collection: string, sku: Array<string>) {
        let options: any = {};
        options.default = 1;
        options._id = 0;
        for (let index = 0; index < sku.length; index++) {
            options[sku[index]] = 1;
        }
        return new Promise((resolve, reject) => {
            this.connection.collection(collection).find({_id: config.tenant}, options).toArray((err: any, result: any) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            });
        });
    }

    public async update(collection: string, deviceProperty: DeviceProperty) {
        return new Promise((resolve, reject) => {
            let query = {
                assetId: deviceProperty.assetId
            };
            this.connection.collection(collection).update({assetId: deviceProperty.assetId},
                {$push: {deviceStatus: deviceProperty.deviceProperties}}, (err: any, result: any) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            });
        });
    }

    public async getDataBasedOnProjectId(projectId: string, collection: string): Promise<any> {
        return new Promise((resolve, reject) => {
            this.connection.collection(collection).find({projectId: projectId}).toArray((err: any, result: any) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            });
        });
    }

    /*public async update(collection: string, deviceProperties: DeviceProperty){
        return new Promise((resolve, reject) => {
            this.connection.collection(collection).update()
    });*/
}