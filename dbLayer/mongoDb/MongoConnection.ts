import {DbConfig} from "../models/tenant";
import {MongoOperation} from "./MongoOperation";
import {IConnection} from "../contracts/Connection";
import {Collection} from "../contracts/Collection";

var MongoClient = require("mongodb").MongoClient;


export class MongoConnection implements IConnection {
    getConnection(dbConfig: DbConfig): Promise<Collection> {
        return new Promise((resolve, reject) => {
            MongoClient.connect(dbConfig.url, function (err: any, db: any) {
                if (err) {
                    reject(err);
                } else {
                    MongoOperation.getInstance().setConnection(db);
                    resolve(MongoOperation.getInstance());
                }
            });
        });
    }
}