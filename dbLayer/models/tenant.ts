export class DbConfig {
    driverName: string;
    url: string;
    buckets: object;
}