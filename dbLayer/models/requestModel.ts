export class DeviceParameters {
    ip: string;
    mac?: Promise<string> | string;
    sku: string;
    oids?: Array<string>;
}