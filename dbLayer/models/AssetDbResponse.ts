import {OidAndValue} from "./oidAndValue";
import {DeviceParameters} from "./requestModel";

export class AssetDbResponse {
  assetProperties: DeviceParameters;
}