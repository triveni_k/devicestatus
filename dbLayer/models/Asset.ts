import {OidAndValue} from "./oidAndValue";

export class Asset {
  assetProperties: any;
  ip: string;
  macAddress: string;
  sku: string;
  oids: object;
  oidsAndValue: OidAndValue

  constructor(ip: string){
    this.ip = ip;
  }
}