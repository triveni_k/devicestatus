import {OidAndValue} from "./oidAndValue";

export class DeviceProperty {
    assetId: string;
    deviceProperties: any;
}