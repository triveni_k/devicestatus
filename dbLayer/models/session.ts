import {OidAndValue} from "./oidAndValue";
export class AssetProperty{
  session: any;
  ip: string;
  oidsAndValue: OidAndValue;
  oids: Array<string>;
  mac: string;
  err: any;
}