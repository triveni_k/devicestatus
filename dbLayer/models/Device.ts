import {OidAndValue} from "./oidAndValue";

export class Device {
        ip: string;
        deviceProperty?: any;
        mac: string;
        sku: string;
        projectId: string;
        assetId: string;
}