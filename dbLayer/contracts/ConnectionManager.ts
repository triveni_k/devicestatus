import {IConnection} from "./Connection";
import {Collection} from "./Collection";
import {DbConfig} from "../models/tenant";

/**
 * Interface for all the connection manger we are going to implement in system
 */
export interface IConnectionManager {
  initConnection(connectionString: string, dbConnection: any): any;
    getConnection(tenant: DbConfig): Promise<Collection>;
}
