import {IConnection} from "./Connection";
import {ConnectionConfiguration} from "../types/DbConfiguration";
import {IConnectionManager} from "./ConnectionManager";
import {Collection} from "./Collection";
import {DbConfig} from "../models/tenant";

export interface ConnectionFactory {
  getConnection(tenant: DbConfig): Promise<Collection>;

  registerManager(driverName: string, manager: IConnectionManager): void;
}