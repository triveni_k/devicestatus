import {DeviceParameters} from "../models/requestModel";

export interface Collection {
   // setCollection(bucket: string, bucketInstance: any): void;
   insert(collection: string, data: Array<DeviceParameters>): Promise<any>;
   getDataBasedOnProjectId(collection: string, projectId: string): Promise<Array<string>>;
}