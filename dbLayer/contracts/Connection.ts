import {Collection} from "./Collection";
import {DbConfig} from "../models/tenant";

export interface IConnection {
    getConnection(tenant: DbConfig): Promise<Collection>;

}