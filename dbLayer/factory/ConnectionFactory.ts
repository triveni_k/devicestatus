import {IConnection} from "../contracts/Connection";
import {ConnectionConfiguration} from "../types/DbConfiguration";
import {IConnectionManager} from "../contracts/ConnectionManager";
import {ConnectionFactory} from "../contracts/ConnectionFactory";
import {Collection} from "../contracts/Collection";
import {DbConfig} from "../models/tenant";

export class IConnectionFactory implements IConnectionFactory {
  private static instance: IConnectionFactory;
  private connectionManagers: Map<string, IConnectionManager>;

  constructor() {
    this.connectionManagers = new Map<string, IConnectionManager>();
  }

  async getConnection(tenant: DbConfig): Promise<Collection> {
    let connection = this.connectionManagers.get(tenant.driverName);
    return connection.getConnection(tenant);
  }

  public registerManager(driverName: string, manager: IConnectionManager) {
    this.connectionManagers.set(driverName, manager);
  }

  static get Instance(): IConnectionFactory {
    return this.instance || (this.instance = new IConnectionFactory());
  }
}