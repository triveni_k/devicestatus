
import {CouchBaseConnection} from "../couchBase/CouchBaseConnection";
import {DbConfig} from "../models/tenant";
import {MongoConnection} from "../mongoDb/MongoConnection";

export class DriverConnectionManager {
    private static instance: DriverConnectionManager;
    registerDriver(db: DbConfig) {
        switch (db.driverName) {
            case "couchbase":
                return  (new CouchBaseConnection());
            case "mongoDb":
                return (new MongoConnection());
        }
    }
    public static getInstance() {
        if (!DriverConnectionManager.instance) {
            DriverConnectionManager.instance = new DriverConnectionManager();
        }
        return DriverConnectionManager.instance;
    }
}