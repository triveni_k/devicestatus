import {DbConfig} from "../models/tenant";
import {IConnection} from "../contracts/Connection";
import {IConnectionManager} from "../contracts/ConnectionManager";
import {Collection} from "../contracts/Collection";

export class ConnectionManager implements IConnectionManager {
    private static instance: ConnectionManager;
    private dbConnections = new Map<string, IConnection>();

    public static getInstance() {
        if (!ConnectionManager.instance) {
            ConnectionManager.instance = new ConnectionManager();
        }
        return ConnectionManager.instance;
    }

    initConnection(connectionString: string, connection: IConnection): any {
        this.dbConnections.set(connectionString, connection);
    }

    async getConnection(tenant: DbConfig): Promise<Collection> {
        let conn = this.dbConnections.get(tenant.driverName);
        return await conn.getConnection(tenant);
    }
}