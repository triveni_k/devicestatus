import {IConnectionFactory} from "../factory/ConnectionFactory";
import {IConnection} from "../contracts/Connection";
import {DbConfig} from "../models/tenant";
import {ConnectionManager} from "./ConnectionManager";
import {Collection} from "../contracts/Collection";
import {DriverConnectionManager} from "../factory/IndexDB";
import {DaoBase} from "../../dao/default/Dao";

let config = require("../../config/config");

export class Register {
    async initIdb() {
        let dbConfig: DbConfig = new DbConfig();
        dbConfig.url = config.db[config.driver].url;
        dbConfig.driverName = config.db[config.driver].driverName;
        dbConfig.buckets = config.db[config.driver].buckets;


        let conn: Collection;
        let connection: IConnection = await DriverConnectionManager.getInstance().registerDriver(dbConfig);
        ConnectionManager.getInstance().initConnection(dbConfig.driverName, connection);
        IConnectionFactory.Instance.registerManager(dbConfig.driverName, ConnectionManager.getInstance());
        //caching a connection since browser sync service is initialised inside the connection.
        conn = await IConnectionFactory.Instance.getConnection(dbConfig);
        DaoBase.getInstance().intConnection(conn);
    }
}

