export interface ConnectionConfiguration {
  driverName: string;
  dbName: string;
}

export interface CollectionConfiguration {
  connectionConfig: ConnectionConfiguration;
  tableName: string;
}