import {DeviceParameters} from "../models/requestModel";
import {DeviceProperty} from "../models/deviceProperties";
import {Device} from "../models/Device";

let config = require("../../config/config");


export class QueryBuilders {
    private static instance: QueryBuilders;

    public static getInstance() {
        if (!QueryBuilders.instance) {
            QueryBuilders.instance = new QueryBuilders();
        }
        return QueryBuilders.instance;
    }

    getDataBasedOnProjectId(id: string, collection: string) {
        return "SELECT * FROM " + collection + " where  projectId=\"" + id + "\";";
    }

    async getDataFromId(collection: string, sku: Array<string>) {
        let skuString = await this.setSku(sku);
        return "SELECT default, " + skuString + " FROM " + collection + " USE KEYS \"" + config.tenant + "\"";
    }

    insert(properties: Array<DeviceParameters>, collection: string) {
        let str: string = "INSERT INTO `" + collection + "` (KEY,VALUE) ";
        for (let i = 0; i < properties.length; i++) {
            str += this.insertStringBuilder(properties[i]);
            str += i !== properties.length - 1 ? "," : ";";
        }
        return str;
    }

    insertStringBuilder(property: any) {
        return "VALUES (\"" + property.assetId + "\"," + JSON.stringify(property) + ")";
    }

    updateStringBuilder(device: DeviceProperty, collection: string) {
        return "UPDATE " + collection + " SET deviceProperties = " + JSON.stringify(device.deviceProperties) +
            " WHERE  assetId = \"" + device.assetId + "\";";
    }

    private setSku(sku: Array<string>) {
        let skuStr: string = "";
        for (let index = 0; index < sku.length; index++) {
            skuStr += sku[index];
            skuStr += index !== sku.length - 1 ? "," : "";
        }
        return skuStr;
    }
}
