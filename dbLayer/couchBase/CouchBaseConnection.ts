import {DbConfig} from "../models/tenant";
import {IConnection} from "../contracts/Connection";
import {CouchBaseOperation} from "./CouchBaseOperation";
import {IConnectionManager} from "../contracts/ConnectionManager";

let couchbase = require("couchbase");

export class CouchBaseConnection extends DbConfig implements IConnection {
    tenant: any;
    bucketConnections = new Map<string, IConnectionManager>();

    static async getBucketConnections(buckets: any, cluster: any) {
        for (let bucket in buckets) {
            if (bucket) {
                let bucketobj = cluster.openBucket(buckets[bucket]);
                CouchBaseOperation.getInstance().setCollection(buckets[bucket], bucketobj);
            }
        }
    }

    async getConnection(dbConfig: DbConfig) {
        try {
            let cluster = new couchbase.Cluster(dbConfig.url);
            await cluster.authenticate("Administrator", "1234567890");
            CouchBaseConnection.getBucketConnections(dbConfig.buckets, cluster);
            return CouchBaseOperation.getInstance();
        } catch (ex) {
            throw ex;
        }
    }
}