import {Collection} from "../contracts/Collection";
import {DeviceParameters} from "../models/requestModel";
import {QueryBuilders} from "./QueryBuilder";
import {IConnectionManager} from "../contracts/ConnectionManager";
import {OidAndValue} from "../models/oidAndValue";
import {DeviceProperty} from "../models/deviceProperties";
import {Device} from "../models/Device";

let couchbase = require("couchbase");

export class CouchBaseOperation implements Collection {
    private static instance: CouchBaseOperation;
    private static collection = new Map<string, IConnectionManager>();

    public static getInstance() {
        if (!CouchBaseOperation.instance) {
            CouchBaseOperation.instance = new CouchBaseOperation();
        }
        return CouchBaseOperation.instance;
    }

    static executeQuery(bucket: any, query: string): any {
        return new Promise((resolve, reject) => {
            bucket.query(query, (err: any, data: any) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(data);
                }
            });
        });
    }

    static async generateQuery(bucket: any, queryInput: string) {
        try {
            let query = await couchbase.N1qlQuery.fromString(queryInput);
            return query.consistency(couchbase.N1qlQuery.Consistency.REQUEST_PLUS);
        } catch (exception) {
            throw exception;
        }
    }

    async createPrimaryIndex(collection: string, coll: IConnectionManager) {
        let query = "CREATE PRIMARY INDEX ON " + collection + " USING GSI;";
        query = await CouchBaseOperation.generateQuery(coll, query);
        return await  CouchBaseOperation.executeQuery(coll, query);
    }

    public async insert(collection: string, data: Array<DeviceParameters>) {
        let coll = CouchBaseOperation.collection.get(collection);
        let query = await QueryBuilders.getInstance().insert(data, collection);
        query = await CouchBaseOperation.generateQuery(coll, query);
        try {
            await CouchBaseOperation.executeQuery(coll, query);
            this.createPrimaryIndex(collection, coll);
        } catch (ex) {
            throw ex;
        }
    }

    async getDataFromId(collection: string, sku: Array<string>) {
        let coll = CouchBaseOperation.collection.get(collection);
        let query = await QueryBuilders.getInstance().getDataFromId(collection, sku);
        query = await CouchBaseOperation.generateQuery(coll, query);
        try {
            return await CouchBaseOperation.executeQuery(coll, query);
        } catch (ex) {
            return ex;
        }

    }

    public async getDataBasedOnProjectId(id: string, collection: string) {
        let coll = CouchBaseOperation.collection.get(collection);
        let query = await QueryBuilders.getInstance().getDataBasedOnProjectId(id, collection);
        query = await CouchBaseOperation.generateQuery(coll, query);
        try {
            let result: Array<Device> = await CouchBaseOperation.executeQuery(coll, query);
            return this.convertToArray(result);
        } catch (ex) {
            return ex;
        }
    }

    public async update(collection: string, deviceProperty: DeviceProperty) {
        let coll = CouchBaseOperation.collection.get(collection);
        let query = await QueryBuilders.getInstance().updateStringBuilder(deviceProperty, collection);
        query = await CouchBaseOperation.generateQuery(coll, query);
        try {
            return await CouchBaseOperation.executeQuery(coll, query);
        } catch (ex) {
            return ex;
        }
    }

    public setCollection(bucket: string, bucketInstance: any) {
        CouchBaseOperation.collection.set(bucket, bucketInstance);
    }

    private convertToArray(devices: Array<any>) {
        let res: Array<any> = [];
        for (let index = 0; index < devices.length; index++) {
            res.push(devices[index].DeviceStatus);
        }
        return res;
    }
}
