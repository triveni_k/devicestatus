1. JWT authorization - It has been added with feature flag. If feature flag is disabled then authorization.


2. build script- It will run both typescript compilation and tslint. Command to build the solution is - *npm run debug*

3. Release script - added a release command which will compile all the code and paste compiled code in a output directory just like as dotnet and java. Output folder name will be build, This folder will only have compiled code so 
   we don't need to deploy our source code, we will just deploy compiled code. Command to create release folder is - *npm run release*


Editing API definition in swagger 

1. Install swagger if it is not installed in your system -> npm install swagger -g
1. Go to root folder of application.
2. open git bash/cmd 
3. enter command - swagger project edit
4. This will open a window in chrome. You can edit API definition. Edited definition will be saved in api/swagger.yaml file.