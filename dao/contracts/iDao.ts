import {DeviceParameters} from "../../dbLayer/models/requestModel";
import {OidAndValue} from "../../dbLayer/models/oidAndValue";
import {Device} from "../../dbLayer/models/Device";
import {DeviceProperty} from "../../dbLayer/models/deviceProperties";

export interface IDaoBase {
    insert(data: Array<DeviceParameters>): void;
    update(data: DeviceProperty): void;
    getAllData(projectId: string): Promise<Array<Device>>;
    getDataFromSku(sku: Array<string>): Promise<any>;
}