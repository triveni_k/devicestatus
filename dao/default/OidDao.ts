import {DaoBase} from "./Dao";
let config = require("../../config/config");

export class OidDao extends DaoBase {
    constructor() {
        super(config.collections.Oids);
    }
}