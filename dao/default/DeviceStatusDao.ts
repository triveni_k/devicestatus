import {DaoBase} from "./Dao";
import {Collection} from "../../dbLayer/contracts/Collection";
let config = require("../../config/config");

export class DeviceStatusDao extends DaoBase {
    connection: Collection;
    constructor() {
        super(config.collections.DeviceStatus);
    }

}