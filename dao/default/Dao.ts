import {DeviceParameters} from "../../dbLayer/models/requestModel";
import {IDaoBase} from "../contracts/iDao";
import {DeviceProperty} from "../../dbLayer/models/deviceProperties";
import {Collection} from "../../dbLayer/contracts/Collection";
import {Device} from "../../dbLayer/models/Device";

export class DaoBase implements IDaoBase {
    private static instance: DaoBase;
    protected collection: any;
    protected static connection: any;

    constructor(collection?: string) {
        this.collection = collection;
    }

    public static getInstance() {
        if (!DaoBase.instance) {
            DaoBase.instance = new DaoBase();
        }
        return DaoBase.instance;
    }

    intConnection(connection: Collection) {
        DaoBase.connection = connection;
    }
    async getDataFromSku(ids: Array<string>) {
        return await DaoBase.connection.getDataFromId(this.collection, ids);
    }
    async insert(data: Array<DeviceParameters>) {
        try {
            await DaoBase.connection.insert(this.collection, data);
        } catch (ex) {
            throw ex;
        }
    }
    async getAllDataBasedOnProjectId(id: string) {
        return await DaoBase.connection.getDataBasedOnProjectId(id, this.collection);
    }

    update(data: DeviceProperty) {
        DaoBase.connection.update(this.collection, data);
    }

    async getAllData() {
        return await DaoBase.connection.getAllData(this.collection);
    }
}