/**
 * Created by manish.kumawat on 12/26/2017.
 */
var gulp = require('gulp');
var ts = require('gulp-typescript');
var tsProject = ts.createProject('tsconfig.json', {noImplicitAny: true});
var sourcemaps = require('gulp-sourcemaps');
var merge = require('merge2');
var path = require('path');
var tslint = require("gulp-tslint");

// gulpfile.js

var gulpCopy = require('gulp-copy');
var sourceFiles = ["**/*.json","api/**/*.*","dbLayer/**/*.*","dao/**/**/*.*", "config/**/*.*", "api/swagger/swagger.yaml", "!config/**/*.ts", "!config/**/*.d.ts", "!config/**/*.js.map",
  "!tsconfig.json", "!tslint.json", "!package.json", "!coverage/**/*.*", "!backup/**/*.*", "!build/**/*.*",
  "node_modules/**/*.*", "!test/**/*.*"];
var destination = "build/";

gulp.task('ts', function () {
  var tsResult = gulp.src(["**/*.ts", "!**/*.d.ts", "!backup/**/*.*", "!build/**/*.*", "!node_modules/**/*.*"], {base: '.'})
      .pipe(sourcemaps.init())
      .pipe(tsProject());

  return merge([
    tsResult.dts.pipe(gulp.dest('.')),
    tsResult.js.pipe(sourcemaps.write('.')).pipe(gulp.dest('.')),
    tsResult.pipe(sourcemaps.write('.'))
  ]);
});

gulp.task('solutionBuild', function () {
  var tsResult = gulp.src(["**/*.ts", "!**/*.d.ts", "!backup/**/*.*", "!build/**/*.*", "!node_modules/**/*.*"], {base: '.'})
      .pipe(tsProject());

  return merge([
    tsResult.js.pipe(sourcemaps.write('.')).pipe(gulp.dest('build')),
  ]);
});

gulp.task('copy', function () {
  gulp.src(sourceFiles, {base: "."})
      .pipe(gulp.dest('build/'));
});

gulp.task('watch', ['ts', 'tslint'], function () {
  gulp.watch(['server/**/*.ts', 'worker/**/*.ts'], ['ts']);
  gulp.watch('./api/swagger.yaml', ['swagger']);
});

gulp.task("tslint", function () {
  gulp.src(["**/*.ts", "!**/*.d.ts", "!backup/**/*.*", "!build/**/*.*", "!node_modules/**/*.*"])
      .pipe(tslint({
        formatter: "prose"
      })).pipe(tslint({
    configuration: "tslint.json"
  })).pipe(tslint.report({
    emitError: true,
    reportLimit: 0,
    summarizeFailureOutput: false,
    allowWarnings: true
  }))
});

gulp.task('default', ['ts']); // Run default