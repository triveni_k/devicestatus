/*jslint node: true, nomen: true, plusplus: true*/
"use strict";

require("winston-daily-rotate-file");

var winston = require("winston"),
    Mail = require("winston-mail").Mail,
    fs = require("fs"),

    config = require("./config.js").log;


winston.loggers.add("logger", {
  transports: [
//TODO: need to uncomment the code once the configuration is fully functional.
    /*  new winston.transports.Mail({
     to: config.mailTo,
     from: config.mailFrom,
     host: config.hostName,
     username: config.userName,
     password: config.password,
     subject: config.subject,
     level: config.errorLogLevel,
     handleExceptions: true
     }).on("error", function (err) { //Handles errors if exception happens, in case of server or internet is down
     log.debug(err.stack);
     }),
     */
    //to log all the levels in the console
    new (winston.transports.Console)(
        {
          level: config.debugLogLevel,
          colorize: true
        }),

    //new files will be generated each day, the date patter indicates the frequency of creating a file.
    new winston.transports.DailyRotateFile({
          name: "debug-log",
          filename: config.debugLogFileName,
          level: config.debugLogLevel,
          prepend: true,
          datePattern: config.datePattern,
          maxFiles: config.maxFiles
        }
    ),
    new (winston.transports.DailyRotateFile)({
      name: "error-log",
      level: config.errorLogLevel,
      filename: config.errorLogFilename,
      prepend: true,
      datePattern: config.datePattern,
      maxFiles: config.maxFiles
    })
  ]
});


//creates a directory if the directory is not present
exports.createLogDirectory = function () {
  if (!fs.existsSync(config.logDirectory)) {
    fs.mkdirSync(config.logDirectory);
  }
};


var logger = winston.loggers.get("logger");
Object.defineProperty(exports, "LOG", {value: logger});
