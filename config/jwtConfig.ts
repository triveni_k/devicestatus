var config = require("./config").jwtConfig;

export class JwtConfig {
  /**
   * Return jwt config
   * @param configType
   * @returns {any}
   */
  static getJWTConfiguration(configType: string) {
    return config[configType];
  }
}
