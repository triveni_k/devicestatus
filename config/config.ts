var path = require("path");
//var ProviderType = require(path.normalize(__dirname + "/../dbLayer/enums/providerType")).dbLayer.ProviderType;
var rootPath = path.normalize(__dirname + "/../");

module.exports = {
    "certKeyFilePath": "/config/sample.key",
    "certFilePath": "/config/sample.crt",
    "serverConfig": {
        "honorCipherOrder": true,
        "secureProtocol": "TLSv1_2_method",
        "port": 9010,
        "timeout": 60000
    },
    //defaultDbProvider: ProviderType.MongoDb,
    apiPaths: {
        version1: "/api/v1",
        excludeAuthentication: []
    },
    deviceService: {
        snmp: "snmp"
    },
    tenant: "hp",
    deviceStatus: true,
    driver: "mongoDb",
    db: {
        couchbase: {
            driverName: "couchbase",
            isEnabled: true,
            url: "couchbase://localhost",
            buckets: ["DeviceStatus", "Oids"]
        },
        mongoDb: {
            driverName: "mongoDb",
            isEnabled: false,
            url: "mongodb://localhost/asset-properties",
        }
    },
    dbCredentials: {
        user: "0aa70a1998fbf1a211d20a7dbbc2d51b",
        password: "429cc93786b6a9ec58fd7bbd38942bb1"
    },
    log: {
        eventType: "EventLogger",
        errorLogLevel: "error",
        errorLogFilename: __dirname + "/logs/_error.log",
        debugLogLevel: "debug",
        debugLogFileName: rootPath + "/logs/_debug.log",
        datePattern: "MM-d-yyyy",
        maxFiles: 5, //only 5 days log files  will be generated
    },
    jwtConfig: {
        cartos_itt: {
            //TODO: use a new super secret for enterprise setup during deployment
            secret: "VWZqntcVCnvep8K1dP1smBtsSy7HPbSNByNnXvVPsm8",
            expireTime: 60 * 60 * 24, //token expires in 24 hours
            audience: "cartos_itt",
            issuer: "https://cartosdesign.com"
        },
        cartos_tenant: {
            //TODO: use a new super secret for enterprise setup during deployment
            secret: "PY7CSY0kLJ2o6VMcj1fTqs83rUJOyyORVNvKF76injI",
            expireTime: 60 * 60 * 24, //token expires in 24 hours
            audience: "cartos_tenant",
            issuer: "http://cartosdesign.com"
        },
        cartos_local: {
            //TODO: use a new super secret for enterprise setup during deployment
            secret: "PY7CSY0kLJ2o6VMcj1fTqs83rSAOyyORVNvKF76injI",
            expireTime: 60 * 60 * 24, //token expires in 24 hours
            audience: "cartos_local",
            issuer: "http://cartosdesign.com"
        }
    },
    collections: {
        DeviceStatus: "DeviceStatus",
        Oids: "Oids"
    },
    RedFortServiceBaseUrl: "https://localhost:9003/api/v1",
    ssl: {
        rejectUnAuthorizedCert: false
    },
    features: {
        authorization: true
    }
};