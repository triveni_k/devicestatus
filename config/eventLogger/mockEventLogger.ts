/*
 Class:
 MockEventLogger: Invoked not to log any user details.
 Methods:
 emitEnterEvent: Invoked to enter an event but not to log,
 emitExitEvent: Invoked to exit an event but not to log.
 Parameters:
 customInfo: provides complete info of a log,
 log: used to log the info,
 objectType: provides complete path,
 result: provides success & failure details,
 type: provides type of Operation,
 userId: provides userId info.
 */

import {OperationType} from "../../enums/enum";

let logger = require("../logger"),
    log = logger.LOG;

let customInfo: string;

export class MockEventLogger {
  public emitEnterEvent(userId: number, type: OperationType, objectType: string, result: any) {
    //anything you would like
  }

  public emitExitEvent(userId: number, type: OperationType, objectType: string, result: any) {
    //anything you would like
  }
}
