/*
 Class:
 EventLogger: Invoked to log an event.
 Methods:
 emitEnterEvent: Invoked to enter an event,
 emitExitEvent: Invoked to exit an event,
 formatLogMessage: Invoked to append an customInfo,
 info: Invoked to log an event.

 Parameter:
 config: used to load config file,
 customInfo: provides complete info of a log,
 log: used to log the info,
 objectType: provides complete path,
 result: provides success & failure details,
 type: provides type of Operation,
 userId: provides userId info.
 */

import {OperationType} from "../../enums/enum";

let config = require("../config");
let customInfo: string;
let logger = require("../logger"),
    log = logger.LOG;

export class EventLogger {
  public emitEnterEvent(userId: number, type: OperationType, objectType: string, result: any) {
    customInfo = "Entry - ";
    this.info(userId, type, objectType, result);
  }

  public emitExitEvent(userId: number, type: OperationType, objectType: string, result: any) {
    customInfo = "Exit - ";
    this.info(userId, type, objectType, result);
  }

  private info(userId: number, type: OperationType, objectType: string, result: any) {
    this.formatLogMessage(userId, type, objectType, result);
    log.info(customInfo);
  }

  private formatLogMessage(userId: number, type: OperationType, objectType: string, result: any) {
    if (userId) {
      customInfo += "U: " + userId;
    }
    if (type) {
      customInfo += ", O: " + type;
    }
    if (objectType.indexOf("?") > 0) {
      customInfo += ", T: " + objectType.substring(objectType.indexOf(objectType.replace(config.apiPaths.version1,
                  "")) + 1, objectType.indexOf("?"));
    } else if (objectType) {
      customInfo += ", T: " + objectType.substring(objectType.indexOf(objectType.replace(config.apiPaths.version1, ""))
              + 1, objectType.lastIndexOf(""));
    }
    if (result) {
      if (result.data && result.err) {
        customInfo += ", P_RESULT: {" + result.data.toString() + "}, ERR: {" + result.err.toString() + "}";
      } else if (result.data && !result.err) {
        customInfo += ", RESULT: {" + result.data.toString() + "}";
      } else if (result.err) {
        customInfo += ", ERR: {" + result.err.toString() + "}";
      }
    }

  }
}