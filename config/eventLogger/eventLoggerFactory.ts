/*
 Class:
 EventLoggerFactory: Invoked to create an object on calling.
 Methods:
 getEventLogger: Invoked to create object of specific class.
 Parameters:
 client: used to load the required file,
 config: used to load config file,
 constants: used to load constants file,
 eventType: contains event info ie.., to log an event or not.
 */

import {EventType} from "../../constants/constant";

let client: any;


export class EventLoggerFactory {
  public static getEventLogger(eventType?: string) {
    if (eventType === undefined) {
      eventType = EventType.EventLogger;
    }
    switch (eventType) {
      case  EventType.EventLogger: {
        client = require("./eventLogger").EventLogger;
      }
        break;
      case EventType.None: {
        client = require("./mockEventLogger").MockEventLogger;
      }
        break;
      default: {
        client = require("./mockEventLogger").MockEventLogger;
      }
    }
    return client;
  }
}
