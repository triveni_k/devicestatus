"use strict";

import {ErrorFactory} from "../../errorHandler/errorFactory";

import {Snmp} from "../../DeviceServiceConnection/default/Snmp";
import {RegisterDeviceProxy} from "../helpers/RegisterDevice/proxy";


let snmp = new Snmp();
let registerDeviceProxy = new RegisterDeviceProxy();

let util = require("util");
export class RegisterDevice {


    async registerDevice(req: any, res: any) {
            await registerDeviceProxy.registerDevice(req, res);
    }

}
let regDevice = new RegisterDevice();
module.exports = {
    registerDevice: regDevice.registerDevice
};