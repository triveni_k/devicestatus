import {RegisteredDeviceProxy} from "../helpers/RegisteredDevices/proxy";
import {RegisterDevice} from "./registerDevice";

let regDevicesProxy = new RegisteredDeviceProxy();

export class RegisteredDevices {
     getAllRegisteredDevices(req: any, res: any) {
          regDevicesProxy.getAllRegisteredDevices(req, res);
    }
}
let registeredDevices = new RegisteredDevices();
module.exports = {
    RegisteredDevices: registeredDevices.getAllRegisteredDevices
};