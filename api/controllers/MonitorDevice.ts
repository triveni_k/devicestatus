import {MonitorDeviceService} from "../helpers/MonitorDevices/MonitorDevice";
import {Snmp} from "../../DeviceServiceConnection/default/Snmp";
import {MonitorDevicesProxy} from "../helpers/MonitorDevices/proxy";
import {RegisterDevice} from "./registerDevice";

let monitorDevicesProxy = new MonitorDevicesProxy();
let snmp = new Snmp();

class MonitorDevice {
    async monitorDevice(req: any, res: any) {
        monitorDevicesProxy.monitorDevices(req, res);
    }
}

let monitorDevice = new MonitorDevice();
module.exports = {
    MonitorDevice: monitorDevice.monitorDevice
};
