import {Register} from "../../../dbLayer/default/Register";
import {Device} from "../../../dbLayer/models/Device";
import {SnmpFactory} from "../../../DeviceServiceConnection/default/SnmpFactory";
import {DeviceProperty} from "../../../dbLayer/models/deviceProperties";
import {DeviceStatusDao} from "../../../dao/default/DeviceStatusDao";
import {OidDao} from "../../../dao/default/OidDao";
import {OidAndValue} from "../../../dbLayer/models/oidAndValue";
import {ResModel} from "../../../dbLayer/models/ResModel";

let snmpFactory = new SnmpFactory();
let config = require("../../../config/config");
let register = new Register();

export class MonitorDeviceService {
    private deviceProperties = new Map<string, string>();

    async monitorDevices(projectId: string) {
        try {
            let deviceService = await this.getDeviceService();
            let {deviceStatusDao, devices} = await this.getRegisteredAsset(projectId);
            let skuInArr: Array<string> = this.getAllSkuInArray(devices);
            let oidsJson = await this.getOids(skuInArr);
            return await this.getDeviceStatus(oidsJson, deviceService, devices);
        } catch (ex) {
            throw ex;
        }
    }

    mapAndGenerateOidData(oidData: any) {
        let oids: Array<string> = [];
        for (let key in oidData) {
            if (oidData.hasOwnProperty(key)) {
                oids.push(key);
                this.deviceProperties.set(key, oidData[key]);
            }
        }
        return oids;
    }

    private getAllSkuInArray(devices: Array<Device>) {
        let sku: Array<string> = [];
        for (let index = 0; index < devices.length; index++) {
            if (!(sku.indexOf(devices[index].sku) > -1)) {
                sku.push(devices[index].sku);
            }
        }
        return sku;
    }

    private async getRegisteredAsset(projectId: string) {
        let deviceStatusDao = new DeviceStatusDao();
        let devices: Array<Device> = await deviceStatusDao.getAllDataBasedOnProjectId(projectId);
        return {deviceStatusDao, devices};
    }

    private async getAllValidData(ip: string, oids: Array<string>, deviceService: any) {
        let deviceProperties: any = {};
        for (let index = 0; index < oids.length; index++) {
            try {
                let properties = await deviceService.getDeviceProperties(ip, oids[index]);
                deviceProperties[oids[index]] = properties[oids[index]];
            } catch (ex) {
                continue;
            }
            return deviceProperties;
        }
    }

    private async getDeviceService() {
        snmpFactory.registerService(config.deviceService.snmp);
        return await snmpFactory.getInstances(config.deviceService);
    }

    private mapOid(oidsAndValue: any) {
        let result: any = {}
        for (let key in oidsAndValue) {
            if (oidsAndValue.hasOwnProperty(key)) {
                result[this.deviceProperties.get(key)] = oidsAndValue[key];
            }
        }
        return result;
    }

    private getOid(oidsAndValue: any) {
        let oids: Array<string> = [];
        for (let key in oidsAndValue[0].Oids) {
            if (oidsAndValue[0].Oids.hasOwnProperty(key)) {
                oids.push(key);
                this.deviceProperties.set(key, oidsAndValue[0].Oids[key]);
            }
        }
        return oids;
    }

    private async getOids(sku: Array<string>) {
        let oidDao = new OidDao();
        let x = await oidDao.getDataFromSku(sku);
        console.log(x);
        return x;
    }

    private async getDeviceStatus(oidsJson: any, deviceService: any, devices: Array<Device>) {
        let res: any = {}, deviceProperties;
        for (let index = 0; index < devices.length; index++) {
            let deviceStatusDao = new DeviceStatusDao();
            let oids: Array<string> = this.mapAndGenerateOidData(oidsJson[0].default);
            oids = oids.concat(this.mapAndGenerateOidData(oidsJson[0][devices[index].sku]));
            await deviceService.createSnmpCon(devices[index].ip);
            try {
                deviceProperties = await deviceService.getDeviceProperties(devices[index].ip, oids);
                deviceProperties = this.mapDeviceProperties(deviceProperties);
            } catch (ex) {
                deviceProperties = await this.getAllValidData(
                    devices[index].ip, oids, deviceService);
                res[devices[index].assetId].deviceProperties = deviceProperties;
                deviceStatusDao.update({"assetId": devices[index].assetId, "deviceProperties": deviceProperties});
                continue;
            }
            res[devices[index].assetId] = deviceProperties;
            deviceStatusDao.update({"assetId": devices[index].assetId, "deviceProperties": deviceProperties});
        }
        return res;
    }

    private mapDeviceProperties(deviceProperties: any) {
        let res: any = {};
        for (let key in deviceProperties) {
            if (deviceProperties.hasOwnProperty(key)) {
                res[this.deviceProperties.get(key)] = deviceProperties[key];
            }
        }
        return res;
    }
}
