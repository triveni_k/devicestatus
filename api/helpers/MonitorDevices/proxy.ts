import {MonitorDeviceService} from "./MonitorDevice";
import {DeviceProperty} from "../../../dbLayer/models/deviceProperties";
import {ErrorFactory} from "../../../errorHandler/errorFactory";
let monitorDeviceService = new MonitorDeviceService();
export class MonitorDevicesProxy {
    async monitorDevices(req: any, res: any) {
        try {
            res.send(await monitorDeviceService.monitorDevices(req.query.projectId));
        } catch (ex) {
            res.send(ErrorFactory.getErrors(ex));
        }
    }
}
