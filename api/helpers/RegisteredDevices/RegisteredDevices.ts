import {DeviceStatusDao} from "../../../dao/default/DeviceStatusDao";
import {Device} from "../../../dbLayer/models/Device";

export class RegisteredDevices {
    async getAllRegisteredDevices(id: string) {
        try {
            let deviceStatus = new DeviceStatusDao();
            let data = await deviceStatus.getAllDataBasedOnProjectId(id);
            return this.mapIp(data);
        } catch (ex) {
            throw ex;
        }
    }

    mapIp(data: Array<Device>) {
        let res: any = {};
        for (let index = 0; index < data.length; index++) {
            res[data[index].assetId] = data[index];
        }
        return res;
    }
}