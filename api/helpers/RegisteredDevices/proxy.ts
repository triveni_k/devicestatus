import {RegisteredDevices} from "./RegisteredDevices";

let regDevices = new RegisteredDevices();

export class RegisteredDeviceProxy {
    async getAllRegisteredDevices(req: any, res: any) {
        try {
            res.send(await regDevices.getAllRegisteredDevices(req.query.projectId));
        } catch (ex) {
            throw ex;
        }
    }
}