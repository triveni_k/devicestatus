import {Register} from "../../../dbLayer/default/Register";
import {DeviceStatusDao} from "../../../dao/default/DeviceStatusDao";
import {DeviceParameters} from "../../../dbLayer/models/requestModel";

let config = require("../../../config/config");
var macfromip = require("macfromip");
let register = new Register();

export class RegisterDevice {
    async registerDevice(req: any) {
        try {
            let devStatus = new DeviceStatusDao();
            await this.mapMacAddr(req.body);
            await devStatus.insert(req.body);
        } catch (ex) {
            throw ex;
        }
    }

    async mapMacAddr(devicePar: Array<DeviceParameters>) {
        let deviceParameters: Array<DeviceParameters> = devicePar;
        for (let i = 0; i < devicePar.length; i++) {
            if (!devicePar[i].mac) {
                deviceParameters[i].mac = await this.getMac(devicePar[i].ip);
            }
        }
    }

    getMac(ip: string): Promise<string> {
        return new Promise((resolve, reject) => {
            macfromip.getMac(ip, function (err: any, data: any) {
                if (err) {
                    reject(err);
                } else {
                    resolve(data);
                }
            });
        });
    }
}