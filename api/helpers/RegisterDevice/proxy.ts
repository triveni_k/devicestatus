import {RegisterDevice} from "./RegisterDevice";
import {ErrorFactory} from "../../../errorHandler/errorFactory";

let registerDevice = new RegisterDevice();

export class RegisterDeviceProxy {
    async registerDevice(req: any, res: any) {
        try {
            await registerDevice.registerDevice(req);
            res.send(req.body.length + " records updated");
        } catch (ex) {
            res.status(400);
            res.send(ErrorFactory.getErrors(ex));
        }
    }
}